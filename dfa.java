import java.io.*;
class dfa
{
static BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
static int n1,ttable[][],fstate[],f1,istate,store_state;
public static void construct_dfa() throws Exception
{

System.out.println("\nTransition states are by default taken 0/1 \n The states should be represented in integer values (state 0 to state n)\nHow many states are there?");
n1=Integer.parseInt(br.readLine());
ttable=new int[n1][2];// Declaring the state transition table

System.out.println("\n\nHow many final states are there?");
f1=Integer.parseInt(br.readLine()); 

fstate=new int[f1];

System.out.println("\nEnter the initial state");
istate=Integer.parseInt(br.readLine());



System.out.println("\nEnter the final state(s)");

for(int i=0;i<f1;i++)
fstate[i]=Integer.parseInt(br.readLine());

System.out.println("\nNow enter the values for the state transition table \n");
for(int i=0;i<n1;i++)
{
for(int j=0;j<2;j++)
{
System.out.println("Enter value for state = "+i+" sigma = "+j);
ttable[i][j]=Integer.parseInt(br.readLine());
}
System.out.println();
}

System.out.println("\nYour DFA is constructed\n");
}

static void check_dfa() throws Exception
{

store_state=istate;
int t=0;
        System.out.println("Enter a string");
        String x=br.readLine();
        for(int i=0;i<x.length();i++)
        {
            char b=x.charAt(i);
            t=Integer.parseInt(String.valueOf(b));
            store_state=ttable[store_state][t];
        }
        
        System.out.println("The state reached is "+store_state);
        int k=0;
        for(int i=0;i<f1;i++)
        if(store_state==fstate[i])
        {
            System.out.println(" which is a final state");
            k++;
        }
        
        if(k==0)
            System.out.println(" which is not a final state");
}

public static void main(String args[]) throws Exception
{
int c=0;
construct_dfa();
System.out.println("\nNow you can validate the strings whether it supports the DFA\n");
check_dfa();
System.out.println("\nPRess Enter to continue");
String aa=br.readLine();

for(;;)
{
System.out.println("\nPress 1 to continue checking some more strings with the same DFA\nPress 2 to exit\n");
c=Integer.parseInt(br.readLine());
if(c==1)

check_dfa();
else
System.exit(0);
}

}
}
